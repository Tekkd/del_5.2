using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nurses_R_US.Dtos 
{
    public class UserDto
    {
    public int UserId { get; set; }
    public string UserName { get; set; }
    public string UserNormalizedName { get; set; }
    public DateTime DateCreated { get; set; }
    public string Email { get; set; }
    public string NormalisedEmail { get; set; }
    public int? EmailConfirmed { get; set; }
    public byte[] PasswordHash { get; set; }
    public byte[] PasswordSalt { get; set; }
    public string SecurityStamp { get; set; }
    public string ConcurrencyStamp { get; set; }
    public string PhoneNumber { get; set; }
    public int? PhoneNumberConfirms { get; set; }
    public int? TwoFactorEnabled { get; set; }
    public DateTime? LockoutEnd { get; set; }
    public int? LockoutEnabled { get; set; }
    public int? AccessFailedCount { get; set; }
    public string Discriminator { get; set; }
    public string FullName { get; set; }
  }
}
