using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Nurses_R_US.Models
{
    public partial class EmployeeStatus
    {
        public EmployeeStatus()
        {
            Employee = new HashSet<Employee>();
        }

    [Key]
        public int EmpSId { get; set; }
        public string EmpSName { get; set; }
        public string EmpSDescription { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
    }
}
