using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class SkillType
    {
        public SkillType()
        {
            Skill = new HashSet<Skill>();
        }

    [Key]
        public int SkillTId { get; set; }
        public string SkillTName { get; set; }
        public string SkillTDescription { get; set; }

        public virtual ICollection<Skill> Skill { get; set; }
    }
}
