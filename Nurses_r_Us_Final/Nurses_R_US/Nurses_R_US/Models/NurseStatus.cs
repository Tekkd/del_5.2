using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class NurseStatus
    {
        public NurseStatus()
        {
            Nurse = new HashSet<Nurse>();
        }

    [Key]
        public int NurseStatusId { get; set; }
        public string NurseStatusName { get; set; }

        public virtual ICollection<Nurse> Nurse { get; set; }
    }
}
