using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class NurseSkill
    {
    [Key]
        public int NurseId { get; set; }
        public int SkillId { get; set; }
        public DateTime? DateObtained { get; set; }

    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    [ForeignKey("SkillId")]
        public virtual Skill Skill { get; set; }
    }
}
