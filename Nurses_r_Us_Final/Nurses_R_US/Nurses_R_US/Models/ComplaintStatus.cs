using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Nurses_R_US.Models
{
    public partial class ComplaintStatus
    {
        public ComplaintStatus()
        {
            Complaint = new HashSet<Complaint>();
        }

    [Key]
        public int CmpltStatusId { get; set; }
        public string CmpltStatusName { get; set; }
        public string CmpltStatusDescription { get; set; }

        public virtual ICollection<Complaint> Complaint { get; set; }
    }
}
