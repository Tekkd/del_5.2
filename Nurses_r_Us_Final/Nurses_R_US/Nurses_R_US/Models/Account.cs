using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Nurses_R_US.Models
{
    public partial class Account
    {
    [Key]
        public int AccountId { get; set; }
        public string AccountNumber { get; set; }
        public string AccountHolder { get; set; }
        public int BankId { get; set; }
        public int AccountTypeId { get; set; }
        public int NurseId { get; set; }
    [ForeignKey("AccountTypeId")]
        public virtual AccountType AccountType { get; set; }
    [ForeignKey("BankId")]
        public virtual Bank Bank { get; set; }
    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    }
}
