using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 

namespace Nurses_R_US.Models
{
    public partial class OrientationType
    {
        public OrientationType()
        {
            Orientation = new HashSet<Orientation>();
        }

    [Key]
        public int OtId { get; set; }
        public string OtName { get; set; }
        public string OtDescription { get; set; }

        public virtual ICollection<Orientation> Orientation { get; set; }
    }
}
