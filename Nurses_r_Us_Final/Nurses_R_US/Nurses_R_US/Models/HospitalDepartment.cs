using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class HospitalDepartment
    {
        public HospitalDepartment()
        {
            HdRate = new HashSet<HdRate>();
            Request = new HashSet<Request>();
        }

    [Key]
        public int HospDeptId { get; set; }
        public string HdaName { get; set; }
        public string HdaFname { get; set; }
        public string HdaLname { get; set; }
        public string HdaEmail { get; set; }
        public string HdaCellphone { get; set; }
        public string HdaTelephone { get; set; }
        public int HospId { get; set; }
        public int TitleId { get; set; }
        public int HdsId { get; set; }

    [ForeignKey("HdsId")]
        public virtual HospitalDepartmentStatus Hds { get; set; }
    [ForeignKey("HospId")]
        public virtual Hospital Hosp { get; set; }
    [ForeignKey("TitleId")]
        public virtual Title Title { get; set; }
        public virtual ICollection<HdRate> HdRate { get; set; }
        public virtual ICollection<Request> Request { get; set; }
    }
}
