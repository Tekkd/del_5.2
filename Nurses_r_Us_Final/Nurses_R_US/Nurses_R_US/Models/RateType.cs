using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 

namespace Nurses_R_US.Models
{
    public partial class RateType
    {
        public RateType()
        {
            Rate = new HashSet<Rate>();
        }

    [Key]
        public int RateTypeId { get; set; }
        public string RateTypeName { get; set; }
        public string RateTypeDescription { get; set; }

        public virtual ICollection<Rate> Rate { get; set; }
    }
}
