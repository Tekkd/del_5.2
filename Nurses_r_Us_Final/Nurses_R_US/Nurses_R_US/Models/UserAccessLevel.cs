using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class UserAccessLevel
    {
    [Key]
        public int AccessLevelId { get; set; }
    [Key]
        public int UserId { get; set; }

    [ForeignKey("AccessLevelId")]
        public virtual AccessLevel AccessLevel { get; set; }
    [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
