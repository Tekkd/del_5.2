using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Hospital
    {
        public Hospital()
        {
            HospitalDepartment = new HashSet<HospitalDepartment>();
        }

    [Key]
        public int HospId { get; set; }
        public string HospName { get; set; }
        public string HospTelephone { get; set; }
        public string HospEmail { get; set; }
        public decimal? HospLongitude { get; set; }
        public decimal? HospLatitude { get; set; }
        public string HospAddress { get; set; }

        public virtual ICollection<HospitalDepartment> HospitalDepartment { get; set; }
    }
}
