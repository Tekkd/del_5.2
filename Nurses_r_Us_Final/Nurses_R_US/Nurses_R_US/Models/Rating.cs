using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 

namespace Nurses_R_US.Models
{
    public partial class Rating
    {
        public Rating()
        {
            NurseAllocation = new HashSet<NurseAllocation>();
        }

    [Key]
        public int RatingId { get; set; }
        public decimal? RatingLevel { get; set; }
        public string RatingDescription { get; set; }

        public virtual ICollection<NurseAllocation> NurseAllocation { get; set; }
    }
}
