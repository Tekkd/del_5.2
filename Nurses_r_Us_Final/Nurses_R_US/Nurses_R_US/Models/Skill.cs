using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Skill
    {
        public Skill()
        {
            NurseSkill = new HashSet<NurseSkill>();
            QualificationSkill = new HashSet<QualificationSkill>();
            Request = new HashSet<Request>();
        }

    [Key]
        public int SkillId { get; set; }
        public string SkillName { get; set; }
        public string SkillDescription { get; set; }
        public int SkillTId { get; set; }

    [ForeignKey("SkillTId")]
        public virtual SkillType SkillT { get; set; }
        public virtual ICollection<NurseSkill> NurseSkill { get; set; }
        public virtual ICollection<QualificationSkill> QualificationSkill { get; set; }
        public virtual ICollection<Request> Request { get; set; }
    }
}
