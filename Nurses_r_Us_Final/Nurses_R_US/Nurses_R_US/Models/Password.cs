using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Password
    {
    [Key]
        public int PasswordId { get; set; }
        public string Password1 { get; set; }
        public int UserId { get; set; }
        public int LoginProvider { get; set; }
        public string ProviderDisplayName { get; set; }

    [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
