using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Nurses_R_US.Models
{
    public partial class EmployeementHistory
    {
    [Key]
        public string EhId { get; set; }
        public string Employer { get; set; }
        public string Position { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public int NurseId { get; set; }

    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    }
}
