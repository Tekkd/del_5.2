using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nurses_R_US.Models
{
    public partial class ComplaintType
    {
        public ComplaintType()
        {
            Complaint = new HashSet<Complaint>();
        }

    [Key]
        public int CmpltTypeId { get; set; }
        public string CmpltTypeName { get; set; }
        public string CmpltTypeDescription { get; set; }

        public virtual ICollection<Complaint> Complaint { get; set; }
    }
}
