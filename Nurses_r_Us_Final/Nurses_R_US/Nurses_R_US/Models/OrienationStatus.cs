using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class OrienationStatus
    {
        public OrienationStatus()
        {
            Orientation = new HashSet<Orientation>();
        }

    [Key]
        public int OsId { get; set; }
        public string OsName { get; set; }
        public string OsDescription { get; set; }

        public virtual ICollection<Orientation> Orientation { get; set; }
    }
}
