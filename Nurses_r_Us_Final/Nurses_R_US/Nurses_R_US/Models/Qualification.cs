using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Qualification
    {
        public Qualification()
        {
            QualificationSkill = new HashSet<QualificationSkill>();
        }

    [Key]
        public int QualId { get; set; }
        public string QualName { get; set; }
        public string QualDescription { get; set; }
        public DateTime QualDate { get; set; }
        public int TiId { get; set; }

    [ForeignKey("TiId")]
        public virtual TertiaryInstitution Ti { get; set; }
        public virtual ICollection<QualificationSkill> QualificationSkill { get; set; }
    }
}
