using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nurses_R_US.Models
{
    public partial class HospitalDepartmentStatus
    {
        public HospitalDepartmentStatus()
        {
            HospitalDepartment = new HashSet<HospitalDepartment>();
        }

    [Key]
        public int HdsId { get; set; }
        public string HdsName { get; set; }

        public virtual ICollection<HospitalDepartment> HospitalDepartment { get; set; }
    }
}
