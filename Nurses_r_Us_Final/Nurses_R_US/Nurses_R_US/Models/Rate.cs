using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Rate
    {
        public Rate()
        {
            HdRate = new HashSet<HdRate>();
        }

    [Key]
        public int DepartmentRateId { get; set; }
        public decimal? RateAmount { get; set; }
        public string DeptRateName { get; set; }
        public string DeptRateDescription { get; set; }
        public int RateTypeId { get; set; }

    [ForeignKey("RateTypeId")]
        public virtual RateType RateType { get; set; }
        public virtual ICollection<HdRate> HdRate { get; set; }
    }
}
