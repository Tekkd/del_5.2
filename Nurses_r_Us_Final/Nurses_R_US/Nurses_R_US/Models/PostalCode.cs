  using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 

namespace Nurses_R_US.Models
{
    public partial class PostalCode
    {
        public PostalCode()
        {
            Location = new HashSet<Location>();
        }
    [Key]
        public int PosCode { get; set; }
        public string PosCodeName { get; set; }

        public virtual ICollection<Location> Location { get; set; }
    }
}
