using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class NurseDocument
    {
    [Key]
        public int NdId { get; set; }
        public string NdName { get; set; }
        public string NdPath { get; set; }
        public int NurseId { get; set; }

    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    }
}
