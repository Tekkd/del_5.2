using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Branch
    {
        public Branch()
        {
            Employee = new HashSet<Employee>();
            Nurse = new HashSet<Nurse>();
        }

    [Key]
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int LocationId { get; set; }

    [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
    }
}
