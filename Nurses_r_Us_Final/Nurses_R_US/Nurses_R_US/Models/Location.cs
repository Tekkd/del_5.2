using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Location
    {
        public Location()
        {
            Branch = new HashSet<Branch>();
            Employee = new HashSet<Employee>();
            Nurse = new HashSet<Nurse>();
            TertiaryInstitution = new HashSet<TertiaryInstitution>();
        }

    [Key]
        public int LocationId { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public int PosCode { get; set; }
        public int SuburbId { get; set; }
        public int CityId { get; set; }

    [ForeignKey("CityId")]
        public virtual City City { get; set; }
    [ForeignKey("PosCode")]
        public virtual PostalCode PosCodeNavigation { get; set; }
    [ForeignKey("SuburbId")]
        public virtual Suburb Suburb { get; set; }
        public virtual ICollection<Branch> Branch { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
        public virtual ICollection<TertiaryInstitution> TertiaryInstitution { get; set; }
    }
}
