using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Request
    {
        public Request()
        {
            Geolocation = new HashSet<Geolocation>();
            NurseAllocation = new HashSet<NurseAllocation>();
        }

    [Key]
        public int RequestId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int NoNurse { get; set; }
        public int ShiftTypeId { get; set; }
        public int ReqStatusId { get; set; }
        public int SkillId { get; set; }
        public int RankId { get; set; }
        public int HospDeptId { get; set; }

    [ForeignKey("HospDeptId")]
        public virtual HospitalDepartment HospDept { get; set; }
    [ForeignKey("RankId")]
        public virtual NurseRank Rank { get; set; }
    [ForeignKey("ReqStatusId")]
        public virtual RequestStatus ReqStatus { get; set; }
    [ForeignKey("ShiftTypeId")]
        public virtual ShiftType ShiftType { get; set; }
    [ForeignKey("SkillId")]
        public virtual Skill Skill { get; set; }
        public virtual ICollection<Geolocation> Geolocation { get; set; }
        public virtual ICollection<NurseAllocation> NurseAllocation { get; set; }
    }
}
