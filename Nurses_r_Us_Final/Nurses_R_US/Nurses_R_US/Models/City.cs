using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class City
    {
        public City()
        {
            Location = new HashSet<Location>();
        }

    [Key]
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int ProvId { get; set; }

    [ForeignKey("ProvId")]
        public virtual Province Prov { get; set; }
        public virtual ICollection<Location> Location { get; set; }
    }
}
