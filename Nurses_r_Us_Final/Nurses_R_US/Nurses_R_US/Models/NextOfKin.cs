using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class NextOfKin
    {
    [Key]
        public int NokId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public decimal? Cellphone { get; set; }
        public string Email { get; set; }
        public int NurseId { get; set; }

    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    }
}
