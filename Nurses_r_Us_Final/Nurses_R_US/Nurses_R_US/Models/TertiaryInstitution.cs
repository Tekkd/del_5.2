using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class TertiaryInstitution
    {
        public TertiaryInstitution()
        {
            Qualification = new HashSet<Qualification>();
        }

    [Key]
        public int TiId { get; set; }
        public string Name { get; set; }
        public int LocationId { get; set; }

    [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual ICollection<Qualification> Qualification { get; set; }
    }
}
