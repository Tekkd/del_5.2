using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nurses_R_US.Models
{
    public partial class Employee
    {
    [Key]
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string EmpSurname { get; set; }
        public string EmpCellphone { get; set; }
        public string EmpEmail { get; set; }
        public DateTime EmpDob { get; set; }
        public int TitleId { get; set; }
        public int EmpTypeId { get; set; }
        public int UserId { get; set; }
        public int LocationId { get; set; }
        public int BranchId { get; set; }
        public int EmpSId { get; set; }

    [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
    [ForeignKey("EmpSId")]
        public virtual EmployeeStatus EmpS { get; set; }
    [ForeignKey("EmpTypeId")]
        public virtual EmployeeType EmpType { get; set; }
    [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
    [ForeignKey("TitleId")]
        public virtual Title Title { get; set; }
    [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
