using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class AccessLevelClaim
    {
    [Key]
        public int AlcId { get; set; }
        public string AlcClaimType { get; set; }
        public string AlcValue { get; set; }
        public int AccessLevelId { get; set; }
    [ForeignKey("AccessLevelId")]
        public virtual AccessLevel AccessLevel { get; set; }
    }
}
