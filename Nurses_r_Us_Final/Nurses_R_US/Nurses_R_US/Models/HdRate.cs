using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class HdRate
    {
    [Key]
        public int HospDeptId { get; set; }
    [Key]
        public int RateId { get; set; }
        public DateTime? Date { get; set; }

    [ForeignKey("HospDeptId")]
        public virtual HospitalDepartment HospDept { get; set; }
    [ForeignKey("RateId")]
        public virtual Rate Rate { get; set; }
    }
}
