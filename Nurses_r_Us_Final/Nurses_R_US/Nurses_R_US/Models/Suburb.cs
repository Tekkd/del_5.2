using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 

namespace Nurses_R_US.Models
{
    public partial class Suburb
    {
        public Suburb()
        {
            Location = new HashSet<Location>();
        }

    [Key]
        public int SuburbId { get; set; }
        public string SuburbName { get; set; }

        public virtual ICollection<Location> Location { get; set; }
    }
}
