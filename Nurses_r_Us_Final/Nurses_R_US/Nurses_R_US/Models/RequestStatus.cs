using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class RequestStatus
    {
        public RequestStatus()
        {
            Request = new HashSet<Request>();
        }

    [Key]
        public int ReqStatusId { get; set; }
        public string ReqStatusName { get; set; }

        public virtual ICollection<Request> Request { get; set; }
    }
}
