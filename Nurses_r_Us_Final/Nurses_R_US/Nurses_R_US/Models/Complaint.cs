using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nurses_R_US.Models
{
    public partial class Complaint
    {
        public Complaint()
        {
            NurseAllocation = new HashSet<NurseAllocation>();
        }

    [Key]
        public int CmpltId { get; set; }
        public string CmpltDescription { get; set; }
        public int CmpltStatusId { get; set; }
        public int CmpltTypeId { get; set; }

    [ForeignKey("CmpltStatusId")]
        public virtual ComplaintStatus CmpltStatus { get; set; }
    [ForeignKey("CmpltTypeId")]
        public virtual ComplaintType CmpltType { get; set; }
        public virtual ICollection<NurseAllocation> NurseAllocation { get; set; }
    }
}
