using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class UserToken
    {
    [Key]
        public int UserTokenId { get; set; }
        public string LoginProvider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int UserId { get; set; }

    [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
