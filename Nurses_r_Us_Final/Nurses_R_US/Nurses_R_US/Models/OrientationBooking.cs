using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class OrientationBooking
    {
    [Key]
        public int NurseId { get; set; }
        public int OrientId { get; set; }
        public int OrientBSId { get; set; }
        public DateTime? DateTime { get; set; }
        public TimeSpan? TimeIn { get; set; }
        public TimeSpan? TimeOut { get; set; }

    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    [ForeignKey("OrientId")]
        public virtual Orientation Orient { get; set; }
    [ForeignKey("OrientBSId")]
        public virtual OrientationBookingStatus OrientBS { get; set; }
    }
}
