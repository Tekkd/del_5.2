using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class NurseAllocation
    {
    [Key]
        public int NurseId { get; set; }
        public string BookingConfirmation { get; set; }
    [Key]
        public int CmpltId { get; set; }
    [Key]
        public int RequestId { get; set; }
    [Key]
        public int RatingId { get; set; }

    [ForeignKey("CmpltId")]
        public virtual Complaint Cmplt { get; set; }
    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    [ForeignKey("RatingId")]
        public virtual Rating Rating { get; set; }
    [ForeignKey("RequestId")]
        public virtual Request Request { get; set; }
    }
}
