using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class QualificationSkill
    {
    [Key]
        public int SkillId { get; set; }
        public int QualId { get; set; }

    [ForeignKey("SkillId")]
        public virtual Qualification Qual { get; set; }
    [ForeignKey("QualId")]
        public virtual Skill Skill { get; set; }
    }
}
