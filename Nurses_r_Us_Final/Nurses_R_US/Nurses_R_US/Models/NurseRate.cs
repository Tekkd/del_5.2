using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class NurseRate
    {
    [Key]
        public int RatesId { get; set; }
        public string RatesAmount { get; set; }
        public DateTime? Date { get; set; }
        public int NurseId { get; set; }

    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    }
}
