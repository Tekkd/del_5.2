using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class ActiveUser
    {
    [Key]
        public int ActUserId { get; set; }
        public int UserTokenId { get; set; }
        public TimeSpan? SessionTime { get; set; }
        public int UserId { get; set; }
    [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
