using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace Nurses_R_US.Models
{
    public partial class Geolocation
    {
    [Key]
        public int GeoLoId { get; set; }
        public decimal GeoloLongitude { get; set; }
        public decimal GeoloLatitude { get; set; }
        public TimeSpan CheckIn { get; set; }
        public TimeSpan CheckOut { get; set; }
        public int NurseId { get; set; }
        public int RequestId { get; set; }

    [ForeignKey("NurseId")]
        public virtual Nurse Nurse { get; set; }
    [ForeignKey("RequestId")]
        public virtual Request Request { get; set; }
    }
}
