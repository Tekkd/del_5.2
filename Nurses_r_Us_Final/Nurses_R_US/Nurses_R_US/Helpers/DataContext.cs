using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nurses_R_US.Models; 

namespace Nurses_R_US.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }

    // Fluent API Decleration
    protected override void OnModelCreating (ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<QualificationSkill>()
        .HasKey(c => new { c.QualId, c.SkillId });

      modelBuilder.Entity<HdRate>()
        .HasKey(b => new { b.RateId, b.HospDeptId });

      modelBuilder.Entity<NurseAllocation>()
        .HasKey(d => new { d.NurseId, d.CmpltId, d.RequestId, d.RatingId });

      modelBuilder.Entity<UserAccessLevel>()
        .HasKey(e => new { e.AccessLevelId, e.UserId }); 

    }

    // Fluent API Decleration
    public DbSet<Nurses_R_US.Models.Title> Title { get; set; }
    }
}
