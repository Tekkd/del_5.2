import { Component, OnInit } from '@angular/core';
import { QualificationsService } from 'src/app/Services/qualifications.service';
import { ToastrService } from 'ngx-toastr';
import { Qualifications } from 'src/app/Models/qualifications';

@Component({
  selector: 'app-search-qualification',
  templateUrl: './search-qualification.component.html',
  styleUrls: ['./search-qualification.component.scss']
})
export class SearchQualificationComponent implements OnInit {

  constructor(private QualificationService: QualificationsService,
              private toastr: ToastrService ) { }

  ngOnInit() {
    this.QualificationService.refreshListQualification();
  }
  // tslint:disable-next-line: no-shadowed-variable
  populateForm(Qualifications: Qualifications) {
    this.QualificationService.formData = Object.assign({}, Qualifications);
  }

  onDelete(id: number) {
    if (confirm('Are you sure you would like to delete the following record ?')) {
    this.QualificationService.DeleteQualification(id).subscribe(res => {
      this.QualificationService.refreshListQualification();
      this.toastr.info('Qualification Deleted Successfully');
     });
   }
  }
}
