import { Postal } from 'src/app/Models/postal.model';
import { Component, OnInit } from '@angular/core';
import { QualificationsService } from 'src/app/Services/qualifications.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-qualification',
  templateUrl: './qualification.component.html',
  styleUrls: ['./qualification.component.scss']
})
export class QualificationComponent implements OnInit {

  constructor(public QualificationService: QualificationsService,
              public toastr: ToastrService) { }

  ngOnInit() {
    this.QualificationService.refreshListQualification();
  }
  restForm(form?: NgForm) {
    if (form != null) {
    form.resetForm();
    }
    this.QualificationService.formData = {
    Qual_ID: null,
    Qual_name: '',
    Qual_Description: '',
    TI_ID: null
    };
  }

  onSubmit(form: NgForm) {
    if (form.value.Qual_ID == null) {
    this.insertRecord(form);
    } else {
    this.updateRecord(form);
    }
  }

  insertRecord(form: NgForm) {
    this.QualificationService.PostQualification(form.value).subscribe(res => {
      this.toastr.success('Qualification Inserted Successfully');
      this.restForm(form);
      this.QualificationService.refreshListQualification();
    });
  }

  updateRecord(form: NgForm) {
    this.QualificationService.PutQualification(form.value).subscribe(res => {
      this.toastr.warning('Qualification has been updated successfully');
      this.restForm(form);
      this.QualificationService.refreshListQualification();
    });
  }

}
