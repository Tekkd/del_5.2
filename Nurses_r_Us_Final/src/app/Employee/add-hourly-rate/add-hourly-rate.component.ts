import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { DocumentService } from 'src/app/Services/document.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-add-hourly-rate',
  templateUrl: './add-hourly-rate.component.html',
  styleUrls: ['./add-hourly-rate.component.scss'],
  providers: [DocumentService]
})
export class AddHourlyRateComponent implements OnInit {
  fileToUpload: File = null;
  imageUrl = '/assets/img/';
  constructor(public imageService: DocumentService, toastrService: ToastrService) { }

  ngOnInit() {
  }
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    // Show Image1 Preview
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }
  onSubmit(form: NgForm) {
    this.imageService.postDocument('1', this.fileToUpload).subscribe();
  }

}
