import { Location } from './../../Models/location.model';
import { AccountTypeService } from './../../Services/account-type.service';
import { NurseSkillService } from './../../Services/nurse-skill.service';
import { DocumentService } from './../../Services/document.service';
import { Bank } from './../../Models/bank.model';
import { NurseService } from './../../Services/nurse.service';
import { Postal } from './../../Models/postal.model';
import { Province } from './../../Models/province.Model';
import { City } from './../../Models/city.model';
import { Title } from './../../Models/title.model';
import { Nurse } from './../../Models/nurse.model';
import { Observable } from 'rxjs';
import { NextOfKinService } from './../../Services/next-of-kin.service';
import { ToastrService } from 'ngx-toastr';
import { RankService } from 'src/app/Services/rank.service';
import { NgForm } from '@angular/forms';
import { PostalCodeService } from 'src/app/Services/postal-code.service';
import { CityService } from 'src/app/Services/city.service';
import { TitleService } from 'src/app/Services/title.service';
import { ProvinceService } from 'src/app/Services/province.service';
import { EmploymentHistoryService } from 'src/app/Services/employment-history.service';
import { BankService } from 'src/app/Services/bank.service';
import { LocationService } from 'src/app/Services/location.service';
import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/Services/account.service';
import { TertiaryInstitutionService } from 'src/app/Services/tertiary-institution.service';
import { SkillService } from 'src/app/Services/skill.service';
import { Rank } from 'src/app/Models/rank.model';
import { Skill } from 'src/app/Models/skill';
import { TertiaryInstitution } from 'src/app/Models/tertiary-institution.model';


@Component({
  selector: 'app-apply-to-be-nurse',
  templateUrl: './apply-to-be-nurse.component.html',
  styleUrls: ['./apply-to-be-nurse.component.scss'],
  providers: [DocumentService]
})
export class ApplyToBeNurseComponent implements OnInit {
  imageUrl = '/assets/img/  ';
  fileToUpload: File = null;
  fileToUpload1: File = null;
  fileToUpload2: File = null;

  // new Nurse Object
  location: Location;
  nurse: Nurse;
  constructor( public nursService: NurseService,
               public locatnService: LocationService,
               public nextOKinService: NextOfKinService,
               public bnkService: BankService,
               public accService: AccountService,
               public empHistService: EmploymentHistoryService,
               public provService: ProvinceService,
               public tertInstService: TertiaryInstitutionService,
               public titlService: TitleService,
               public citiService: CityService,
               public postService: PostalCodeService,
               public skilService: SkillService,
               public rankService: RankService,
               public toastr: ToastrService,
               public documentService: DocumentService,
               private nurseSkillService: NurseSkillService,
               public accountTypeService: AccountTypeService) { }

  ngOnInit() {
    this.resetForm();
    this.titlService.refreshtitleList();
    this.rankService.refreshRankList();
    this.provService.refreshProvList();
    this.postService.refreshPostalList();
    this.tertInstService.refreshTertInsList();
    this.skilService.refreshSkillList();
    this.bnkService.refreshBankList();
    this.citiService.refreshCityList();
    this.accountTypeService.refreshListAT();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
    form.resetForm();
    }
    this.nursService.nurseData = {
      Nurse_ID: null,
      Nurse_FName: '',
      Nurse_LName: '',
      Nurse_Transport: '',
      Nurse_Cellphone: '',
      Nurse_Email: '',
      Tax_Number: '',
      Nurse_DOB: null,
      Title_ID: null,
      Rank_ID: null,
      Location_ID: null,
      Branch_ID: null,
      Nurse_Status_ID: null,
      User_ID: null
      };

    this.locatnService.locationData = {
      Location_ID: null,
      Building: '',
      Street: '',
      Pos_Code: null,
      Suburb_ID: null,
      City_ID: null
    };

    this.nextOKinService.nokData = {
      Nok_ID: null,
      Name: '',
      Surname: '',
      Cellphone: '',
      Email: '',
      Nurse_ID: null
    };

    this.bnkService.bankData = {
      Bank_ID: null,
      Bank_Name: '',
      Bank_Code: null
    };

    this.accService.accountData = {
      Account_ID: null,
      Account_Number: '',
      Account_Holder: '',
      Bank_ID: null,
      Account_Type_ID: null,
      Nurse_ID: null
    };

    this.empHistService.EmpHistData = {
      EH_ID: null,
      Employer: '',
      Position: '',
      Date_Start: null,
      Date_End: null,
      Nurse_ID: null
    };

    this.tertInstService.TertInsData = {
      TI_ID: null,
      Name: '',
      Location_ID: null
    };
    this.skilService.skillData = {
      Skill_ID: null,
      Skill_Name: '',
      Skill_Description: '',
      Skill_T_ID: null
    };
  }


  handleFile1Input(file: FileList) {
    this.fileToUpload = file.item(0);

    // Show Image1 Preview
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }

  handleFile2Input(file: FileList) {
    this.fileToUpload1 = file.item(0);

    // Show Image2 Preview
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }

  handleFile3Input(file: FileList) {
    this.fileToUpload2 = file.item(0);
    // Show Image3 Preview
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }


  onSubmit(form: NgForm) {
    if (form.value.Nurse_ID == null) {
      this.insertRecord(form);
    }
}
insertRecord(form: NgForm) {
  this.locatnService.PostLocation(form.value)
.subscribe((data: any) => {this.nursService.nurseData.Location_ID = data;
                           this.nursService.postNurse().subscribe((Nurs: any) => {
this.accService.accountData.Nurse_ID = Nurs.Nurse_ID;
this.nextOKinService.nokData.Nurse_ID = Nurs.Nurse_ID;
this.empHistService.EmpHistData.Nurse_ID = Nurs.Nurse_ID;
const Nurse_ID = Nurs.Nurse_ID;
this.nextOKinService.PostNext_of_Kin().subscribe();
this.accService.PostAccount().subscribe();
// tslint:disable-next-line: no-shadowed-variable
this.documentService.postDocument(Nurse_ID, this.fileToUpload).subscribe();
this.documentService.postDocument(Nurse_ID, this.fileToUpload1).subscribe();
this.documentService.postDocument(Nurse_ID, this.fileToUpload2).subscribe();

this.empHistService.PostEmployment_History().subscribe(res => {

      this.toastr.success('We will contact you shortly.', 'Your Application has been successfully Submitted' );
    });
 });
}
);

}

  // this.provService.PostProvince(form.value).subscribe();
    // this.tertInstService.PostTertiary_Institution(form.value).subscribe();
    // this.titlService.PostTitle(form.value).subscribe();
   // this.citiService.PostCity(form.value).subscribe();
   // this.postService.PostPostal_Code(form.value).subscribe();
    // this.skilService.PostSkill(form.value)
    // this.nurseSkillService.postNurseSkill(form.value).subscribe();
}
