import { UserService } from './../User/shared/user.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private userService: UserService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if (localStorage.getItem('userToken') != null) {
        const roles = next.data['roles'] as Array<string>;
        if (roles) {
          const match = this.userService.roleMatch(roles);
          if (match) { return true; } else {
            this.router.navigate(['/forbidden']);
            return false;
          }
        }
        return true;
      }
      this.router.navigate(['/login']);
      return false;
   }
}
