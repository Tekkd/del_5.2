import { NurseSkill } from './../Models/nurse-skill.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NurseSkillService {

  nsData: NurseSkill;
  readonly rootURL: 'https://localhost:44362/api';
  list: NurseSkill[];
  constructor(private http: HttpClient) { }

  postNurseSkill(list: NurseSkill) {
  return this.http.post('https://localhost:44362/api', list);
}

}

