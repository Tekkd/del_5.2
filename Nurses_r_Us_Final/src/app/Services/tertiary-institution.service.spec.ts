import { TestBed } from '@angular/core/testing';

import { TertiaryInstitutionService } from './tertiary-institution.service';

describe('TertiaryInstitutionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TertiaryInstitutionService = TestBed.get(TertiaryInstitutionService);
    expect(service).toBeTruthy();
  });
});
