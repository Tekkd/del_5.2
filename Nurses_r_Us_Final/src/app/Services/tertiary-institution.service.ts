import { TertiaryInstitution } from './../Models/tertiary-institution.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TertiaryInstitutionService {

  TertInsData: TertiaryInstitution;
  tertlist: TertiaryInstitution[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostTertiary_Institution(TertInsData: TertiaryInstitution) {
    return this.http.post('https://localhost:44362/api/Tertiary_Institution', TertInsData);
  }

  refreshTertInsList() {
    this.http.get('https://localhost:44362/api/Tertiary_Institution')
    .toPromise().then(res => this.tertlist = res as TertiaryInstitution[]);
  }

  PutTertiary_Institution(TertInsData: TertiaryInstitution) {
    return this.http.put('https://localhost:44362/api/Tertiary_Institution/' + TertInsData.TI_ID, TertInsData);
   }


}
