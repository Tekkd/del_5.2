import { Injectable } from '@angular/core';
import { Location } from '../Models/location.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  locationData: Location;
  list: Location[];
  readonly rootURL: 'https://localhost:44362/api';


  constructor(private http: HttpClient) { }

  PostLocation(locationData: Location) {
    return this.http.post('https://localhost:44362/api/Locations', locationData);
  }

  refreshLocateList() {
    this.http.get('https://localhost:44362/api/Locations')
    .toPromise().then(res => this.list = res as Location[]);
  }

  PutLocation(locationData: Location) {
    return this.http.put('https://localhost:44362/api/Locations' + locationData.Location_ID, locationData);
   }

}
