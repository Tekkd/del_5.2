import { HttpClient } from '@angular/common/http';
import { ConfirmNurse } from './../Models/confirm-nurse.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfirmNurseService {
  ConfirmData: ConfirmNurse ;
  ConfirmList: ConfirmNurse[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  refreshConfirmNurseList() {
    this.http.get('https://localhost:44362/api/NurseList')
    .toPromise().then(res => this.ConfirmList = res as ConfirmNurse[]);
  }

  PutConfirmNurse(ConfirmData: ConfirmNurse) {
    return this.http.put('https://localhost:44362/api/NurseList/' + ConfirmData.Nurse_ID, ConfirmData);
   }
getRank


}
