import { HttpClient } from '@angular/common/http';
import { AccountType } from 'src/app/Models/account-type.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccountTypeService {
list: AccountType[];
  constructor(private http: HttpClient) { }
  refreshListAT() {
    this.http.get('https://localhost:44362/api/Account_Type')
    .toPromise().then(res => this.list = res as AccountType[]);
  }
}
