import { TestBed } from '@angular/core/testing';

import { ConfirmNurseService } from './confirm-nurse.service';

describe('ConfirmNurseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfirmNurseService = TestBed.get(ConfirmNurseService);
    expect(service).toBeTruthy();
  });
});
