import { Injectable } from '@angular/core';
import { Province } from 'src/app/Models/province.Model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {
  provinceData: Province;
  provincelist: Province[];
  readonly rootURL: 'https://localhost:44362';

  constructor(private http: HttpClient) { }

  PostProvince(provinceData: Province) {
    return this.http.post('https://localhost:44362/api/Provinces', provinceData);
  }

  refreshProvList() {
    this.http.get('https://localhost:44362/api/Provinces')
    .toPromise().then(res => this.provincelist = res as Province[]);
  }

  PutProvince(provinceData: Province) {
    return this.http.put('https://localhost:44362/api/Provinces/' + provinceData.Prov_ID, provinceData);
   }

}
