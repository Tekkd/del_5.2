import { Injectable } from '@angular/core';
import { Title } from 'src/app/Models/title.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  titleData: Title;
  titlelist: Title[];
  readonly rootURL: 'https://localhost:44362';

  constructor(private http: HttpClient) { }

  PostTitle(titleData: Title) {
    return this.http.post( 'https://localhost:44362/api/Titles', titleData);
  }

  refreshtitleList() {
    this.http.get('https://localhost:44362/api/Titles')
    .toPromise().then(res => this.titlelist = res as Title[]);
  }

  PutTitle(titleData: Title) {
    return this.http.put('https://localhost:44362/api/Titles/' + titleData.Title_ID, titleData);
   }

}
