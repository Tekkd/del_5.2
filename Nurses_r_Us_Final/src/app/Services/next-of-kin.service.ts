import { NextOfKin } from '../Models/next-of-kin.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NextOfKinService {

  nokData: NextOfKin;
  list: NextOfKin[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostNext_of_Kin() {
    const body: NextOfKin = {
    Nok_ID: null,
    Name: this.nokData.Name,
    Surname: this.nokData.Surname,
    Cellphone: this.nokData.Cellphone,
    Email: this.nokData.Email,
    Nurse_ID: this.nokData.Nurse_ID
    };
    return this.http.post('https://localhost:44362/api/Next_of_Kin', body);
  }

  refreshNextOKList() {
    this.http.get('https://localhost:44362/api/Next_of_Kin')
    .toPromise().then(res => this.list = res as NextOfKin[]);
  }

  PutNext_of_Kin(nokData: NextOfKin) {
    return this.http.put('https://localhost:44362/api/Next_of_Kin/' + nokData.Nok_ID, nokData);
   }


}
