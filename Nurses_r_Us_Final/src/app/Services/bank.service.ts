import { HttpClient } from '@angular/common/http';
import { Bank } from '../Models/bank.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  bankData: Bank;
  banklist: Bank[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostBank(bankData: Bank) {
    return this.http.post('https://localhost:44362/api/Bank', bankData);
  }

  refreshBankList() {
    this.http.get('https://localhost:44362/api/Bank')
    .toPromise().then(res => this.banklist = res as Bank[]);
  }

  PutBank(bankData: Bank) {
    return this.http.put('https://localhost:44362/api/Bank/' + bankData.Bank_ID, bankData);
   }




}
