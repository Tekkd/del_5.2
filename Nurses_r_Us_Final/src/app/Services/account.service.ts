import { Account } from '../Models/account.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  accountData: Account;
  list: Account[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostAccount() {
    const body: Account = {
      Account_ID: null,
      Account_Number: this.accountData.Account_Number,
      Account_Holder: this.accountData.Account_Holder,
      Bank_ID: this.accountData.Bank_ID,
      Account_Type_ID: this.accountData.Account_Type_ID,
      Nurse_ID: this.accountData.Nurse_ID
    };
    return this.http.post('https://localhost:44362/api/Accounts', body);
    }

  refreshAccountList() {
    this.http.get('https://localhost:44362/api/Accounts')
    .toPromise().then(res => this.list = res as Account[]);
  }

  PutAccount(accountData: Account) {
    return this.http.put('https://localhost:44362/api/Accounts/' + accountData.Account_ID, accountData);
   }


}
