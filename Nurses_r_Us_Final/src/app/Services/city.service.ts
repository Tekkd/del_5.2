import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { City } from 'src/app/Models/city.model';


@Injectable({
  providedIn: 'root'
})
export class CityService {

  cityData: City;
  citylist: City[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostCity(cityData: City) {
    return this.http.post('https://localhost:44362/api/Cities', cityData);
  }

  refreshCityList() {
    this.http.get('https://localhost:44362/api/Cities')
    .toPromise().then(res => this.citylist = res as City[]);
  }

  PutCity(cityData: City) {
    return this.http.put('https://localhost:44362/api/Cities/' + cityData.City_ID, cityData);
   }


}
