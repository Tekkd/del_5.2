import { Injectable } from '@angular/core';
import { Branch } from '../Models/branch.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  formData: Branch;
  list: Branch[];
  readonly rootURL = 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }
  PostBranch(formData: Branch) {
    return this.http.post('https://localhost:44362/api/Branches', formData);
  }

  refreshListBranch() {
    this.http.get('https://localhost:44362/api/Branches')
    .toPromise().then(res => this.list = res as Branch[]);
  }

  PutBranch(formData: Branch) {
    return this.http.put('https://localhost:44362/api/Branches/ ' + formData.Branch_ID, formData);
  }

  DeleteBranch(id: number) {
    return this.http.delete('https://localhost:44362/api/Branches/' + id);
  }
}
