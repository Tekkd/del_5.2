import { TestBed } from '@angular/core/testing';

import { EmploymentHistoryService } from './employment-history.service';

describe('EmploymentHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmploymentHistoryService = TestBed.get(EmploymentHistoryService);
    expect(service).toBeTruthy();
  });
});
