import { Injectable } from '@angular/core';
import { EmployeeType } from '../Models/employee-type.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeTypeService {

  formData: EmployeeType;
  list: EmployeeType[];
  readonly rootURL = 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostEmployee_Type(formData: EmployeeType) {
    return this.http.post(this.rootURL + '/Employee_Type', formData);
  }

  refreshListEmployeeType() {
    this.http.get(this.rootURL + '/Employee_Type')
    .toPromise().then(res => this.list = res as EmployeeType[]);
  }

  putEmployee_Type(formData: EmployeeType) {
    return this.http.put(this.rootURL + '/Employee_Type/' + formData.Emp_Type_ID, formData);
  }

  DeleteEmployee_Type(id: number) {
    return this.http.delete(this.rootURL + '/Employee_Type/' + id);
  }
}
