import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EmploymentHistory } from '../Models/employment-history.model';

@Injectable({
  providedIn: 'root'
})
export class EmploymentHistoryService {

  EmpHistData: EmploymentHistory;
  list: EmploymentHistory[];
  readonly rootURL: 'https://localhost:44362/api';


  constructor(private http: HttpClient) { }

  PostEmployment_History() {
    const body: EmploymentHistory = {
      EH_ID: null,
      Employer: this.EmpHistData.Employer,
      Position: this.EmpHistData.Position,
      Date_Start: this.EmpHistData.Date_Start,
      Date_End: this.EmpHistData.Date_End,
      Nurse_ID: this.EmpHistData.Nurse_ID
    };
    return this.http.post('https://localhost:44362/api/Employment_History', body);
  }

  refreshEmpHistList() {
    this.http.get('https://localhost:44362/api/Employment_History')
    .toPromise().then(res => this.list = res as EmploymentHistory[]);
  }

  PutEmployment_History(EmpHistData: EmploymentHistory) {
    return this.http.put('https://localhost:44362/api/Employment_History/', EmpHistData);
   }



}
