import { TestBed } from '@angular/core/testing';

import { NurseSkillService } from './nurse-skill.service';

describe('NurseSkillService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NurseSkillService = TestBed.get(NurseSkillService);
    expect(service).toBeTruthy();
  });
});
