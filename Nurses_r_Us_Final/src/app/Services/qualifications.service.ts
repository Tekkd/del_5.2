import { Injectable } from '@angular/core';
import { Qualifications } from 'src/app/Models/qualifications';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QualificationsService {
  formData: Qualifications;
  list: Qualifications[];
  readonly rootURL = 'https://localhost:44362/api';
  constructor(private http: HttpClient) { }


  PostQualification(formData: Qualifications) {
    return this.http.post('https://localhost:44362/api/Qualifications', formData);
  }

  refreshListQualification() {
    this.http.get('https://localhost:44362/api/Qualifications')
    .toPromise().then(res => this.list = res as Qualifications[]);
  }

  PutQualification(formData: Qualifications) {
    return this.http.put('https://localhost:44335/api/Qualifications/' + formData.Qual_ID, formData);
  }

  DeleteQualification(id: number) {
    return this.http.delete('https://localhost:44335/api/Qualifications/' + id);
  }
}
