import { Rank } from 'src/app/Models/rank.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RankService {

  rankList: Rank[];
  readonly rootURL: 'https://localhost:44362';
  constructor(private http: HttpClient) { }

  PostNurse_Rank() {
    return this.http.post('https://localhost:44362/api/Nurse_Rank', this.rankList);
  }

  refreshRankList() {
    this.http.get('https://localhost:44362/api/Nurse_Rank')
    .toPromise().then(res => this.rankList = res as Rank[]);
  }

  PutNurse_Rank(rankList: Rank) {
    return this.http.put('https://localhost:44362/api/Nurse_Rank/' + rankList.Rank_ID, rankList);
   }

}
