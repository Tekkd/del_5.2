
import { Injectable } from '@angular/core';
import { Skill } from '../Models/skill';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  skillData: Skill;
  skillList: Skill[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostSkill(skillData: Skill) {
    return this.http.post('https://localhost:44362/api/Skills', skillData);
  }

  refreshSkillList() {
    this.http.get('https://localhost:44362/api/Skills')
    .toPromise().then(res => this.skillList = res as Skill[]);
  }

  PutSkill(skillData: Skill) {
    return this.http.put('https://localhost:44362/api/Skills/' + skillData.Skill_ID, skillData);
   }
}
