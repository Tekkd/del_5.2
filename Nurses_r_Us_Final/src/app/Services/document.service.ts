import { NurseService } from './nurse.service';
import { Document } from './../Models/document.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
postedFile: File;
document: Document;
  constructor(private http: HttpClient, public service: NurseService) { }

  postDocument(id: string, fileUpload: File) {
    const formData: FormData = new FormData();
    formData.append('Document', fileUpload, fileUpload.name);
    formData.append('Nurse_ID', id);
    return this.http.post('https://localhost:44362/api/Upload', formData);
    }
}
