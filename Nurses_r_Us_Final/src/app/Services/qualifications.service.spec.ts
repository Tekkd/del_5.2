import { TestBed } from '@angular/core/testing';

import { QualificationsService } from './qualifications.service';

describe('QualificationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QualificationsService = TestBed.get(QualificationsService);
    expect(service).toBeTruthy();
  });
});
