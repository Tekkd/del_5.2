import { TestBed } from '@angular/core/testing';

import { NextOfKinService } from './next-of-kin.service';

describe('NextOfKinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NextOfKinService = TestBed.get(NextOfKinService);
    expect(service).toBeTruthy();
  });
});
