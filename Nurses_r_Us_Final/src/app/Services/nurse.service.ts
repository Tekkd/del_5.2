import { HttpClient } from '@angular/common/http';
import { Nurse } from 'src/app/Models/nurse.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NurseService {

  nurseData: Nurse;
  readonly rootURL: 'https://localhost:44362/api';
  list: Nurse[];

  constructor(private http: HttpClient) { }

  postNurse() {
    const body: Nurse = {
      Nurse_ID: null,
      Nurse_FName: this.nurseData.Nurse_FName,
  Nurse_LName: this.nurseData.Nurse_LName,
  Nurse_Transport: this.nurseData.Nurse_Transport,
  Nurse_Cellphone: this.nurseData.Nurse_Cellphone,
  Nurse_Email: this.nurseData.Nurse_Email,
  Tax_Number: this.nurseData.Tax_Number,
  Nurse_DOB: this.nurseData.Nurse_DOB,
  Title_ID: this.nurseData.Title_ID,
  Rank_ID: this.nurseData.Rank_ID,
  Location_ID: this.nurseData.Location_ID,
  Branch_ID: this.nurseData.Branch_ID,
  Nurse_Status_ID: this.nurseData.Nurse_Status_ID,
  User_ID: this.nurseData.User_ID

    };
    return this.http.post('https://localhost:44362/api/Nurses', body);
  }

  refreshNurseList() {
    this.http.get('https://localhost:44362/api/Nurses')
    .toPromise().then(res => this.list = res as Nurse[]);
  }

  PutNurse(nurseData: Nurse) {
    return this.http.put('https://localhost:44362/api/Nurses/' + nurseData.Nurse_ID, nurseData);
   }







}
