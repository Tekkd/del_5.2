import { Injectable } from '@angular/core';
import { Employee } from '../Models/employee.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  formData: Employee;
  list: Employee[];
  readonly rootURL = 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

   PostEmployee(formData: Employee) {
    return this.http.post('https://localhost:44362/api/Employee', formData);
  }

  refreshList() {
    this.http.get('https://localhost:44362/api/Employee')
    .toPromise().then(res => this.list = res as Employee[]);
  }

  PutEmployee(formData: Employee) {
    return this.http.put('https://localhost:44362/api/Employee/' + formData.Emp_ID, formData);
  }

  DeleteEmployee(id: number) {
    return this.http.delete('https://localhost:44362/api/Employee/' + id);
  }



}
