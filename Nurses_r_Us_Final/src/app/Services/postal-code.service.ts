import { Postal } from 'src/app/Models/postal.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostalCodeService {

  postalcodeData: Postal;
  postallist: Postal[];
  readonly rootURL: 'https://localhost:44362/api';

  constructor(private http: HttpClient) { }

  PostPostal_Code(postalcodeData: Postal) {
    return this.http.post('https://localhost:44362/api/Postal_Code', postalcodeData);
  }

  refreshPostalList() {
    this.http.get('https://localhost:44362/api/Postal_Code')
    .toPromise().then(res => this.postallist = res as Postal[]);
  }

  PutPostal_Code(postalcodeData: Postal) {
    return this.http.put('https://localhost:44362/api/Postal_Code/' + postalcodeData.Pos_Code, postalcodeData);
   }

}
