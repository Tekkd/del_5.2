export class Skill {
  // tslint:disable-next-line: variable-name
  Skill_ID: number;
  // tslint:disable-next-line: variable-name
  Skill_Name: string;
  // tslint:disable-next-line: variable-name
  Skill_Description: string;
  // tslint:disable-next-line: variable-name
  Skill_T_ID: number;
}
