export class EmploymentHistory {
  EH_ID: number;
  Employer: string;
  Position: string;
  Date_Start: Date;
  Date_End: Date;
  Nurse_ID: number;
}
