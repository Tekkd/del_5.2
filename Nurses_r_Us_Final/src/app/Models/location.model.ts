export class Location {
  Location_ID: number;
  Building: string;
  Street: string;
  Pos_Code: number;
  Suburb_ID: number;
  City_ID: number;
}
