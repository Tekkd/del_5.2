export class Account {
  Account_ID: number;
  Account_Number: string;
  Account_Holder: string;
  Bank_ID: number;
  Account_Type_ID: number;
  Nurse_ID: number;
}
