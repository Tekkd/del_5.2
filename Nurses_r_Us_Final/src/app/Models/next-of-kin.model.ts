export class NextOfKin {
  // tslint:disable-next-line: variable-name
  Nok_ID: number;
  Name: string;
  Surname: string;
  Cellphone: string;
  Email: string;
  // tslint:disable-next-line: variable-name
  Nurse_ID: number;
}
