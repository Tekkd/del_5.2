export class ConfirmNurse {
  Nurse_ID: number;
  Nurse_FName: string;
  Nurse_LName: string;
  Nurse_Transport: string;
  Nurse_Cellphone: string;
  Nurse_Email: string;
  Tax_Number: string;
  Nurse_DOB: Date;
  Title_ID: number;
  Rank_ID: number;
  Location_ID: number;
  Branch_ID: number;
  Nurse_Status_ID: number;
  User_ID: number;
}
