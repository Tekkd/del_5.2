export class Employee {
    Emp_ID : number; 
    Emp_Name : string; 
    Emp_Surname : string; 
    Emp_Cellphone : string; 
    Emp_Email : string; 
    Emp_DOB : Date; 
    Title_ID : number; 
    Emp_Type_ID : number; 
    Location_ID : number; 
    Branch_ID : number
    Emp_S_ID : number; 
}
