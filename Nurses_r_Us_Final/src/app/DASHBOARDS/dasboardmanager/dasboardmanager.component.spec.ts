import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DasboardmanagerComponent } from './dasboardmanager.component';

describe('DasboardmanagerComponent', () => {
  let component: DasboardmanagerComponent;
  let fixture: ComponentFixture<DasboardmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DasboardmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DasboardmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
