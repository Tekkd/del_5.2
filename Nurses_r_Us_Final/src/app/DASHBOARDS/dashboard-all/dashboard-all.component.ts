import { UserService } from './../../User/shared/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-all',
  templateUrl: './dashboard-all.component.html',
  styleUrls: ['./dashboard-all.component.scss']
})
export class DashboardALLComponent implements OnInit {

  constructor(private router: Router, public userService: UserService) { }

  ngOnInit() {
  }

   Logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }
}
