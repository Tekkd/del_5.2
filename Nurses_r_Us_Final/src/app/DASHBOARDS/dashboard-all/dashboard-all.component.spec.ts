import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardALLComponent } from './dashboard-all.component';

describe('DashboardALLComponent', () => {
  let component: DashboardALLComponent;
  let fixture: ComponentFixture<DashboardALLComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardALLComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardALLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
