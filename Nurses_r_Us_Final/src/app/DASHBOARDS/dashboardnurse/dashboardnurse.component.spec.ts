import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardnurseComponent } from './dashboardnurse.component';

describe('DashboardnurseComponent', () => {
  let component: DashboardnurseComponent;
  let fixture: ComponentFixture<DashboardnurseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardnurseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardnurseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
