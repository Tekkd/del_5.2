import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardemployeeComponent } from './dashboardemployee.component';

describe('DashboardemployeeComponent', () => {
  let component: DashboardemployeeComponent;
  let fixture: ComponentFixture<DashboardemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
