import { QualificationListComponent } from './Employee/search-qualification/qualification-list/qualification-list.component';
import { QualificationComponent } from './Employee/search-qualification/qualification/qualification.component';
import { EmployeeTypeService } from 'src/app/Services/employee-type.service';
import { BranchService } from './Services/branch.service';
import { EmployeeTypeComponent } from './Manager/add-employee-type/employee-type/employee-type.component';
import { EmployeeListComponent } from './Manager/add-employee/employee-list/employee-list.component';
import { EmployeeComponent } from './Manager/add-employee/employee/employee.component';
import { EmployeeService } from 'src/app/Services/employee.service';
import { DocumentService } from './Services/document.service';
import { ProvinceService } from 'src/app/Services/province.service';
import { AccountService } from 'src/app/Services/account.service';
import { BankService } from './Services/bank.service';
import { NextOfKinService } from './Services/next-of-kin.service';
import { LocationService } from './Services/location.service';
import { NurseService } from 'src/app/Services/nurse.service';

// /Nurses_R_US Components
import { ApplyToBeNurseComponent } from './Nurse/apply-to-be-nurse/apply-to-be-nurse.component';
import { BookOrientationComponent } from './Nurse/book-orientation/book-orientation.component';
import { CancelOrientationBookingComponent } from './Nurse/cancel-orientation-booking/cancel-orientation-booking.component';
import { SendNurseCheckInDetailsComponent } from './Nurse/send-nurse-check-in-details/send-nurse-check-in-details.component';
import { ConfirmNurseRequestComponent } from './Nurse/confirm-nurse-request/confirm-nurse-request.component';
// tslint:disable-next-line: max-line-length
import { ApplyToBeHospitalDepartmentComponent } from './Hospital_Department/apply-to-be-hospital-department/apply-to-be-hospital-department.component';
import { RequestNursingServicesComponent } from './Hospital_Department/request-nursing-services/request-nursing-services.component';
// tslint:disable-next-line: max-line-length
import { UpdateNursingSservicesRequestComponent } from './Hospital_Department/update-nursing-sservices-request/update-nursing-sservices-request.component';
// tslint:disable-next-line: max-line-length
import { ConfirmHospitalDepartmentBookingComponent } from './Hospital_Department/confirm-hospital-department-booking/confirm-hospital-department-booking.component';
// tslint:disable-next-line: max-line-length
import { ConfirmHospitalDepartmentDepartmentApplicationComponent } from './Employee/confirm-hospital-department-department-application/confirm-hospital-department-department-application.component';
// tslint:disable-next-line: max-line-length
import { MaintainHospitalDepartmentDetailsComponent } from './Employee/maintain-hospital-department-details/maintain-hospital-department-details.component';
// tslint:disable-next-line: max-line-length
import { SearchHospitalDepartmentDetailsComponent } from './Employee/search-hospital-department-details/search-hospital-department-details.component';
import { ArchiveHospitalDepartmentComponent } from './Employee/archive-hospital-department/archive-hospital-department.component';
import { ConfirmNurseApplicationComponent } from './Employee/confirm-nurse-application/confirm-nurse-application.component';
import { UpdateNurseDetailsComponent } from './Employee/update-nurse-details/update-nurse-details.component';
import { SearchNurseComponent } from './Employee/search-nurse/search-nurse.component';
import { ArchiveNurseComponent } from './Employee/archive-nurse/archive-nurse.component';
import { SearchQualificationComponent } from './Employee/search-qualification/search-qualification.component';
import { UpdateQualificationComponent } from './Employee/update-qualification/update-qualification.component';
import { AddSkillComponent } from './Employee/add-skill/add-skill.component';
import { SearchSkillComponent } from './Employee/search-skill/search-skill.component';
import { UpdateSkillComponent } from './Employee/update-skill/update-skill.component';
import { SendNurseRequestComponent } from './Employee/send-nurse-request/send-nurse-request.component';
import { CancelNurseRequestComponent } from './Employee/cancel-nurse-request/cancel-nurse-request.component';
import { NotifyNurseAvailabilityComponent } from './Employee/notify-nurse-availability/notify-nurse-availability.component';
import { CreateOrientationScheduleComponent } from './Employee/create-orientation-schedule/create-orientation-schedule.component';
import { MaintainOrientationScheduleComponent } from './Employee/maintain-orientation-schedule/maintain-orientation-schedule.component';
import { SearchOrientationDetailsComponent } from './Employee/search-orientation-details/search-orientation-details.component';
import { ViewOrientationScheduleComponent } from './Employee/view-orientation-schedule/view-orientation-schedule.component';
import { RecordOrientatedNursesComponent } from './Employee/record-orientated-nurses/record-orientated-nurses.component';
import { AddRatingComponent } from './Hospital_Department/add-rating/add-rating.component';
import { UpdateRatingComponent } from './Hospital_Department/update-rating/update-rating.component';
import { PublishRatingComponent } from './Employee/publish-rating/publish-rating.component';
import { AddHourlyRateComponent } from './Employee/add-hourly-rate/add-hourly-rate.component';
import { UpdateHourlyRateComponent } from './Employee/update-hourly-rate/update-hourly-rate.component';
import { FileAComplaintComponent } from './Hospital_Department/file-a-complaint/file-a-complaint.component';
import { SearchComplaintComponent } from './Hospital_Department/search-complaint/search-complaint.component';
import { DeleteComplaintComponent } from './Hospital_Department/delete-complaint/delete-complaint.component';
import { SearchUserComponent } from './Manager/search-user/search-user.component';
import { AddUserAccessLevelComponent } from './Manager/add-user-access-level/add-user-access-level.component';
import { SearchUserAccessLevelComponent } from './Manager/search-user-access-level/search-user-access-level.component';
import { MaintainUserAccessLevelComponent } from './Manager/maintain-user-access-level/maintain-user-access-level.component';
import { BackUpComponent } from './Manager/back-up/back-up.component';
import { RestoreComponent } from './Manager/restore/restore.component';
// tslint:disable-next-line: max-line-length
import { GenerateActiveDepartmentReportComponent } from './Manager/generate-active-department-report/generate-active-department-report.component';
import { GenerateCRMReportComponent } from './Manager/generate-crm-report/generate-crm-report.component';
import { GenerateInvoiceReportComponent } from './Manager/generate-invoice-report/generate-invoice-report.component';
// tslint:disable-next-line: max-line-length
import { GenerateOrientationAttendanceReportComponent } from './Manager/generate-orientation-attendance-report/generate-orientation-attendance-report.component';
import { GenerateActiveNursesReportComponent } from './Manager/generate-active-nurses-report/generate-active-nurses-report.component';
import { AddEmployeeComponent } from './Manager/add-employee/add-employee.component';
import { SearchEmployeeComponent } from './Manager/search-employee/search-employee.component';
import { UpdateEmployeeDetailsComponent } from './Manager/update-employee-details/update-employee-details.component';
import { ArchiveEmployeeComponent } from './Manager/archive-employee/archive-employee.component';
import { AddEmployeeTypeComponent } from './Manager/add-employee-type/add-employee-type.component';
import { SearchEmployeeTypeComponent } from './Manager/search-employee-type/search-employee-type.component';
import { MaintainEmployeeTypeComponent } from './Manager/maintain-employee-type/maintain-employee-type.component';
import { DeleteEmployeeTypeComponent } from './Manager/delete-employee-type/delete-employee-type.component';
import { DeleteEmployeeComponent } from './Manager/delete-employee/delete-employee.component';





// Addtional Components
import { appRoutes } from './routes';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// tslint:disable-next-line: max-line-length
import { MatIconModule } from '@angular/material';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SignUpComponent } from './User/sign-up/sign-up.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from './User/shared/user.service';
import { SignInComponent } from './User/sign-in/sign-in.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { AuthGuard } from './auth/auth.guard';
import { from, empty } from 'rxjs';
import { AuthInterceptor } from './auth/auth.interceptor';
import { DasboardmanagerComponent } from './DASHBOARDS/dasboardmanager/dasboardmanager.component';
import { DashboardnurseComponent } from './DASHBOARDS/dashboardnurse/dashboardnurse.component';
import { DashboardemployeeComponent } from './DASHBOARDS/dashboardemployee/dashboardemployee.component';
import { DashboardALLComponent } from './DASHBOARDS/dashboard-all/dashboard-all.component';
import { EmploymentHistoryService } from './Services/employment-history.service';
import { TertiaryInstitutionService } from './Services/tertiary-institution.service';
import { TitleService } from 'src/app/Services/title.service';
import { CityService } from 'src/app/Services/city.service';
import { PostalCodeService } from 'src/app/Services/postal-code.service';
import { SkillService } from './Services/skill.service';
import { RankService } from 'src/app/Services/rank.service';
import { AppRoutingModule } from './app-routing.module';
import { EmployeeTypeListComponent } from './Manager/add-employee-type/employee-type-list/employee-type-list.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';








@NgModule({
  declarations: [
    AppComponent,
    ApplyToBeNurseComponent,
    BookOrientationComponent,
    CancelOrientationBookingComponent,
    SendNurseCheckInDetailsComponent,
    ConfirmNurseRequestComponent,
    ApplyToBeHospitalDepartmentComponent,
    RequestNursingServicesComponent,
    UpdateNursingSservicesRequestComponent,
    ConfirmHospitalDepartmentBookingComponent,
    ConfirmHospitalDepartmentDepartmentApplicationComponent,
    MaintainHospitalDepartmentDetailsComponent,
    SearchHospitalDepartmentDetailsComponent,
    ArchiveHospitalDepartmentComponent,
    ConfirmNurseApplicationComponent,
    UpdateNurseDetailsComponent,
    SearchNurseComponent,
    ArchiveNurseComponent,
    SearchQualificationComponent,
    UpdateQualificationComponent,
    AddSkillComponent,
    SearchSkillComponent,
    UpdateSkillComponent,
    SendNurseRequestComponent,
    CancelNurseRequestComponent,
    NotifyNurseAvailabilityComponent,
    CreateOrientationScheduleComponent,
    MaintainOrientationScheduleComponent,
    SearchOrientationDetailsComponent,
    ViewOrientationScheduleComponent,
    RecordOrientatedNursesComponent,
    AddRatingComponent,
    UpdateRatingComponent,
    PublishRatingComponent,
    AddHourlyRateComponent,
    UpdateHourlyRateComponent,
    FileAComplaintComponent,
    SearchComplaintComponent,
    DeleteComplaintComponent,
    SearchUserComponent,
    AddUserAccessLevelComponent,
    SearchUserAccessLevelComponent,
    MaintainUserAccessLevelComponent,
    BackUpComponent,
    RestoreComponent,
    GenerateActiveDepartmentReportComponent,
    GenerateCRMReportComponent,
    GenerateInvoiceReportComponent,
    GenerateOrientationAttendanceReportComponent,
    GenerateActiveNursesReportComponent,
    AddEmployeeComponent,
    SearchEmployeeComponent,
    UpdateEmployeeDetailsComponent,
    ArchiveEmployeeComponent,
    AddEmployeeTypeComponent,
    SearchEmployeeTypeComponent,
    MaintainEmployeeTypeComponent,
    DeleteEmployeeTypeComponent,
    DeleteEmployeeComponent,
    SignUpComponent,
    SignInComponent,
    HomeComponent,
    UserComponent,
    DasboardmanagerComponent,
    DashboardnurseComponent,
    DashboardemployeeComponent,
    DashboardALLComponent,
    EmployeeComponent,
    EmployeeListComponent,
    EmployeeTypeComponent,
    EmployeeTypeListComponent,
    QualificationComponent,
    QualificationListComponent,
    AdminPanelComponent,
    ForbiddenComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    CommonModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    AppRoutingModule


  ],
  providers: [UserService,
   AuthGuard,
    NurseService,
     LocationService,
     NextOfKinService,
    BankService,
    AccountService,
     EmploymentHistoryService,
    ProvinceService,
     TertiaryInstitutionService,
    TitleService,
     CityService,
     PostalCodeService,
    SkillService,
    RankService,
    PostalCodeService,
    DocumentService,
    EmployeeService,
    BranchService,
    EmployeeTypeService,
    {
      provide : HTTP_INTERCEPTORS,
       useClass : AuthInterceptor,
      multi : true
     }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
