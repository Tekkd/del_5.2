import { Component, OnInit } from '@angular/core';
import { EmployeeTypeService } from 'src/app/Services/employee-type.service';
import { EmployeeType } from 'src/app/Models/employee-type.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-type-list',
  templateUrl: './employee-type-list.component.html',
  styleUrls: ['./employee-type-list.component.scss']
})
export class EmployeeTypeListComponent implements OnInit {

  constructor(public service: EmployeeTypeService,
              private toastr: ToastrService) { }




  ngOnInit() {
    this.service.refreshListEmployeeType();
  }

  populateForm(emp_type: EmployeeType) {
    this.service.formData = Object.assign({}, emp_type);
  }

  onDelete(id: number) {
    if (confirm('Are you sure you would like to delete the following record ?')) {
    this.service.DeleteEmployee_Type(id).subscribe(res => {
      this.service.refreshListEmployeeType();
      this.toastr.info('Employee Type Deleted Successfully', 'Add EMP Type');
     });
   }
  }
}

