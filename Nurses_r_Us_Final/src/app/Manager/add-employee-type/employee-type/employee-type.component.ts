import { Component, OnInit } from '@angular/core';
import { EmployeeTypeService } from 'src/app/Services/employee-type.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-type',
  templateUrl: './employee-type.component.html',
  styleUrls: ['./employee-type.component.scss']
})
export class EmployeeTypeComponent implements OnInit {

  constructor(public service: EmployeeTypeService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.restForm();
  }

  restForm(form?: NgForm) {
    if (form != null) {
    form.resetForm();
    }
    this.service.formData = {
      Emp_Type_ID : null,
      Emp_Type_Name: '',
      Emp_Type_Description: ''
    };
  }

  onSubmit(form: NgForm) {
    if (form.value.Emp_Type_ID == null) {
    this.insertRecord(form);
    } else {
    this.updateRecord(form);
    }
  }

  insertRecord(form: NgForm) {
    this.service.PostEmployee_Type(form.value).subscribe(res => {
      this.toastr.success('Employee Type Inserted Successfully', 'Add EMP Type');
      this.restForm(form);
      this.service.refreshListEmployeeType();
    });
  }

  updateRecord(form: NgForm) {
    this.service.putEmployee_Type(form.value).subscribe(res => {
      this.toastr.warning('Employee Type Updated Successfully', 'Add EMP Type');
      this.restForm(form);
      this.service.refreshListEmployeeType();
    });
  }

}
