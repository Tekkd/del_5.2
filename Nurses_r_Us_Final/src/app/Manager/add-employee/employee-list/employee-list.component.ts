import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/Services/employee.service';
import { Employee } from 'src/app/Models/employee.model';



@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  constructor(public employeeService: EmployeeService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.employeeService.refreshList();
  }

  populateForm(employee: Employee) {
    this.employeeService.formData = Object.assign({}, employee);
  }

  onDelete(id: number) {
    if (confirm('Are you sure you would like to delete the following record ?')) {
    this.employeeService.DeleteEmployee(id).subscribe(res => {
      this.employeeService.refreshList();
      this.toastr.info('Employee Deleted Successfully', 'Add EMP Type');
     });
   }
  }
}

