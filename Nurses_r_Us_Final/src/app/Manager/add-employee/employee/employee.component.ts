import { LocationService } from 'src/app/Services/location.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/Services/employee.service';
import { BranchService } from 'src/app/Services/branch.service';
import { EmployeeTypeService } from 'src/app/Services/employee-type.service';
import { PostalCodeService } from 'src/app/Services/postal-code.service';
import { CityService } from 'src/app/Services/city.service';
import { TitleService } from 'src/app/Services/title.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor( public employeeService: EmployeeService,
               public branchService: BranchService,
               public employeeTypeService: EmployeeTypeService,
               public  cityService: CityService,
               private postalCodeService: PostalCodeService,
               public titleService: TitleService,
               private toastr: ToastrService,
               public locationService: LocationService) {}

  ngOnInit() {
    this.restForm();
    this.branchService.refreshListBranch();
    this.employeeTypeService.refreshListEmployeeType();
    this.cityService.refreshCityList();
    this.postalCodeService.refreshPostalList();
    this.titleService.refreshtitleList();

  }
  restForm(form?: NgForm) {
    if (form != null) {
    form.resetForm();
    }
    this.employeeService.formData = {
    Emp_ID : null,
    Emp_Name : '',
    Emp_Surname : '',
    Emp_Cellphone : '',
    Emp_Email : '',
    Emp_DOB : null,
    Title_ID : null,
    Emp_Type_ID : null,
    Location_ID : null,
    Branch_ID : null,
    Emp_S_ID : null
    };
  }
  onSubmit(form: NgForm) {
    if (form.value.Emp_ID == null) {
    this.insertRecord(form);
    } else {
    this.updateRecord(form);
    }
  }
  insertRecord(form: NgForm) {
    this.locationService.PostLocation(form.value).subscribe((data: any) => {
      this.employeeService.formData.Location_ID = data;
      this.employeeService.PostEmployee(form.value).subscribe(res => {
        this.toastr.success('Employee  Inserted Successfully', 'Add EMP Type');
        this.restForm(form);
        this.employeeService.refreshList();
      });
    });

  }
  updateRecord(form: NgForm) {
    this.employeeService.PutEmployee(form.value).subscribe(res => {
      this.toastr.warning('Employee Type Updated Successfully', 'Add EMP Type');
      this.restForm(form);
      this.employeeService.refreshList();
    });
  }
}


