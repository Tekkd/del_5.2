import { ForbiddenComponent } from './forbidden/forbidden.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AppRoutingModule } from './app-routing.module';
import { ApplyToBeNurseComponent } from './Nurse/apply-to-be-nurse/apply-to-be-nurse.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { SignUpComponent } from './User/sign-up/sign-up.component';
import { SignInComponent } from './User/sign-in/sign-in.component';
import { AuthGuard } from './auth/auth.guard';
import { NgModule } from '@angular/core';


export const appRoutes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  {path: 'forbidden', component: ForbiddenComponent, canActivate : [AuthGuard]},
  { path: ' ', component: AdminPanelComponent, canActivate: [AuthGuard], data: { roles: ['Admin']} },
   { path: 'applyNurse', component: ApplyToBeNurseComponent },
   {
       path: 'signup', component: UserComponent,
       children: [{ path: '', component: SignUpComponent }]
   },
   {
       path: 'login', component: UserComponent,
       children: [{ path: '', component: SignInComponent }]
   },
   { path : '', redirectTo: '/login', pathMatch : 'full'}
 ];

export const routingComponent = [ApplyToBeNurseComponent];

