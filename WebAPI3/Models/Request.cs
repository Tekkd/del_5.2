//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPI3.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Request
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Request()
        {
            this.Geolocations = new HashSet<Geolocation>();
            this.Nurse_Allocation = new HashSet<Nurse_Allocation>();
        }
    
        public int Request_ID { get; set; }
        public System.DateTime DateFrom { get; set; }
        public System.DateTime DateTo { get; set; }
        public int No_Nurse { get; set; }
        public int Shift_Type_ID { get; set; }
        public int Req_Status_ID { get; set; }
        public int Skill_ID { get; set; }
        public int Rank_ID { get; set; }
        public int Hosp_Dept_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Geolocation> Geolocations { get; set; }
        public virtual Hospital_Department Hospital_Department { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nurse_Allocation> Nurse_Allocation { get; set; }
        public virtual Nurse_Rank Nurse_Rank { get; set; }
        public virtual Request_Status Request_Status { get; set; }
        public virtual Shift_Type Shift_Type { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
