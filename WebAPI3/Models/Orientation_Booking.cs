//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPI3.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Orientation_Booking
    {
        public int Nurse_ID { get; set; }
        public int Orient_ID { get; set; }
        public int Orient_B_S_ID { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
        public Nullable<System.TimeSpan> Time_In { get; set; }
        public Nullable<System.TimeSpan> Time_Out { get; set; }
    
        public virtual Nurse Nurse { get; set; }
        public virtual Orientation Orientation { get; set; }
        public virtual Orientation_Booking_Status Orientation_Booking_Status { get; set; }
    }
}
