//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPI3.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public int Emp_ID { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_Surname { get; set; }
        public string Emp_Cellphone { get; set; }
        public string Emp_Email { get; set; }
        public System.DateTime Emp_DOB { get; set; }
        public int Title_ID { get; set; }
        public int Emp_Type_ID { get; set; }
        public int Location_ID { get; set; }
        public int Branch_ID { get; set; }
        public int Emp_S_ID { get; set; }
        public string User_ID { get; set; }
    
        public virtual Branch Branch { get; set; }
        public virtual Employee_Status Employee_Status { get; set; }
        public virtual Employee_Type Employee_Type { get; set; }
        public virtual Location Location { get; set; }
        public virtual Title Title { get; set; }
        public virtual User User { get; set; }
    }
}
