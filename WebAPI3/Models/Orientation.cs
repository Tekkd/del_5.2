//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPI3.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Orientation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Orientation()
        {
            this.Orientation_Booking = new HashSet<Orientation_Booking>();
        }
    
        public int Orient_ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Orient_Name { get; set; }
        public Nullable<System.TimeSpan> Orient_Time { get; set; }
        public string Venue { get; set; }
        public int OT_ID { get; set; }
        public int OS_ID { get; set; }
    
        public virtual Orienation_Status Orienation_Status { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orientation_Booking> Orientation_Booking { get; set; }
        public virtual Orientation_Type Orientation_Type { get; set; }
    }
}
