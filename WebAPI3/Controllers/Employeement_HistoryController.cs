﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class Employment_HistoryController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Employment_History
        public IQueryable<Employment_History> GetEmployment_History()
        {
            return db.Employment_History;
        }

    

        // PUT: api/Employment_History/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployment_History(int id, Employment_History employment_History)
        {
          

            if (id != employment_History.EH_ID)
            {
                return BadRequest();
            }

            db.Entry(employment_History).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Employment_HistoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Employment_History
        [ResponseType(typeof(Employment_History))]
        public IHttpActionResult PostEmployment_History(Employment_History employment_History)
        {
            db.Employment_History.Add(employment_History);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (Employment_HistoryExists(employment_History.EH_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = employment_History.EH_ID }, employment_History);
        }

        // DELETE: api/Employment_History/5
        [ResponseType(typeof(Employment_History))]
        public IHttpActionResult DeleteEmployment_History(int id)
        {
            Employment_History employment_History = db.Employment_History.Find(id);
            if (employment_History == null)
            {
                return NotFound();
            }

            db.Employment_History.Remove(employment_History);
            db.SaveChanges();

            return Ok(employment_History);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Employment_HistoryExists(int id)
        {
            return db.Employment_History.Count(e => e.EH_ID == id) > 0;
        }
    }
}