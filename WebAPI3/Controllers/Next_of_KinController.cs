﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class Next_of_KinController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Next_of_Kin
        public IQueryable<Next_of_Kin> GetNext_of_Kin()
        {
            return db.Next_of_Kin;
        }

     

        // PUT: api/Next_of_Kin/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNext_of_Kin(int id, Next_of_Kin next_of_Kin)
        {
            

            if (id != next_of_Kin.Nok_ID)
            {
                return BadRequest();
            }

            db.Entry(next_of_Kin).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Next_of_KinExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Next_of_Kin
        [ResponseType(typeof(Next_of_Kin))]
        public IHttpActionResult PostNext_of_Kin(Next_of_Kin next_of_Kin)
        {
            db.Next_of_Kin.Add(next_of_Kin);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = next_of_Kin.Nok_ID }, next_of_Kin);
        }

        // DELETE: api/Next_of_Kin/5
        [ResponseType(typeof(Next_of_Kin))]
        public IHttpActionResult DeleteNext_of_Kin(int id)
        {
            Next_of_Kin next_of_Kin = db.Next_of_Kin.Find(id);
            if (next_of_Kin == null)
            {
                return NotFound();
            }

            db.Next_of_Kin.Remove(next_of_Kin);
            db.SaveChanges();

            return Ok(next_of_Kin);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Next_of_KinExists(int id)
        {
            return db.Next_of_Kin.Count(e => e.Nok_ID == id) > 0;
        }
    }
}