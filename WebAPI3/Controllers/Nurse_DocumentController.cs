﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;
using System.Configuration;


namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class Nurse_DocumentController : ApiController
    {
        DBModel db = new DBModel();
        [HttpGet]
        public IQueryable<Nurse_Document> Index()
        {
            return db.Nurse_Documents;
        }
        [HttpPost]
        [Route("api/Upload")]
        public HttpResponseMessage Upload()
        {
            string DocumentName = null;
            var httpRequest = HttpContext.Current.Request;
            // upload File
            var postedFile = httpRequest.Files["Document"];
            var Nurse_ID = Convert.ToInt32(httpRequest["Nurse_ID"]);
                // create Custom filename
            DocumentName = new string(Path.GetFileNameWithoutExtension(postedFile.FileName).Take(10).ToArray()).Replace(" ", "-");
            DocumentName = DocumentName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(postedFile.FileName);
            try
            {
                if (postedFile != null)
                {
                    var filePath = HttpContext.Current.Server.MapPath("~/Document/");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }

                    postedFile.SaveAs(filePath + DocumentName);
                }

                //byte[] bytes;
                //using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                //{
                //    bytes = br.ReadBytes(postedFile.ContentLength);
                //}
                using (DBModel db = new DBModel())
                {
                    Nurse_Document nurse_Document = new Nurse_Document()
                    {
                        ND_Name = Path.GetFileName(postedFile.FileName),
                        ND_ContentType = postedFile.ContentType,
                        //ND_Data = bytes,
                        Nurse_ID = Nurse_ID
                    };
                    db.Nurse_Documents.Add(nurse_Document);
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.Created);
                }
            }

            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}