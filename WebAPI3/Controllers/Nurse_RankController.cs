﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class Nurse_RankController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Nurse_Rank
        public IQueryable<Nurse_Rank> GetNurse_Rank()
        {
            return db.Nurse_Rank;
        }

     

        // PUT: api/Nurse_Rank/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNurse_Rank(int id, Nurse_Rank nurse_Rank)
        {

            if (id != nurse_Rank.Rank_ID)
            {
                return BadRequest();
            }

            db.Entry(nurse_Rank).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Nurse_RankExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Nurse_Rank
        [ResponseType(typeof(Nurse_Rank))]
        public IHttpActionResult PostNurse_Rank(Nurse_Rank nurse_Rank)
        {

            db.Nurse_Rank.Add(nurse_Rank);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = nurse_Rank.Rank_ID }, nurse_Rank);
        }

        // DELETE: api/Nurse_Rank/5
        [ResponseType(typeof(Nurse_Rank))]
        public IHttpActionResult DeleteNurse_Rank(int id)
        {
            Nurse_Rank nurse_Rank = db.Nurse_Rank.Find(id);
            if (nurse_Rank == null)
            {
                return NotFound();
            }

            db.Nurse_Rank.Remove(nurse_Rank);
            db.SaveChanges();

            return Ok(nurse_Rank);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Nurse_RankExists(int id)
        {
            return db.Nurse_Rank.Count(e => e.Rank_ID == id) > 0;
        }
    }
}