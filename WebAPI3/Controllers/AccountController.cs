﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    public class AccountController : ApiController
    {
        [Route("api/User/Register")]
        [HttpPost]
        [AllowAnonymous]
        public IdentityResult Register(AccountModel model)
        {
            var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(userStore);
            var user = new ApplicationUser() { UserName = model.UserName, Email = model.Email };
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 8
            };
            IdentityResult result = manager.Create(user, model.Password);
            return result;
        }

        [HttpGet]
        [Route("api/GetUserClaims")]
        public AccountModel GetUserClaims()
        {
            var identityClaims = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identityClaims.Claims;
            AccountModel model = new AccountModel()
            {
                UserName = identityClaims.FindFirst("Username").Value,
                Email = identityClaims.FindFirst("Email").Value,
                FirstName = identityClaims.FindFirst("FirstName").Value,
                LastName = identityClaims.FindFirst("LastName").Value,
                LoggedOn = identityClaims.FindFirst("LoggedOn").Value
            };
            return model;
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("api/forAdminRole")]
        public string forAdminRole()
        {
            return "for admin role";
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        [Route("api/Manager")]
        public string forManagerRole()
        {
            return "For manager role";
        }

        [HttpGet]
        [Authorize(Roles = "Client")]
        [Route("api/forClientRole")]
        public string forClientRole()
        {
            return "For client role";
        }

        [HttpGet]
        [Authorize(Roles = "Nurse")]
        [Route("api/forNurseRole")]
        public string forNurseRole()
        {
            return "For nurse role";
        }

        [HttpGet]
        [Authorize(Roles = "Employee")]
        [Route("api/forEmployeeRole")]
        public string forEmployeeRole()
        {
            return "for employee role";
        }

        [HttpGet]
        [Authorize(Roles = "Applicant")]
        [Route("api/forApplicantRole")]
        public string forApplicantRole()
        {
            return "for applicant role";
        }
       
    }

}
