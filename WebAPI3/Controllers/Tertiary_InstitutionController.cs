﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class Tertiary_InstitutionController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Tertiary_Institution
        public IQueryable<Tertiary_Institution> GetTertiary_Institution()
        {
            return db.Tertiary_Institution;
        }

        // GET: api/Tertiary_Institution/5
        [ResponseType(typeof(Tertiary_Institution))]
        public IHttpActionResult GetTertiary_Institution(int id)
        {
            Tertiary_Institution tertiary_Institution = db.Tertiary_Institution.Find(id);
            if (tertiary_Institution == null)
            {
                return NotFound();
            }

            return Ok(tertiary_Institution);
        }

        // PUT: api/Tertiary_Institution/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTertiary_Institution(int id, Tertiary_Institution tertiary_Institution)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tertiary_Institution.TI_ID)
            {
                return BadRequest();
            }

            db.Entry(tertiary_Institution).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Tertiary_InstitutionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tertiary_Institution
        [ResponseType(typeof(Tertiary_Institution))]
        public IHttpActionResult PostTertiary_Institution(Tertiary_Institution tertiary_Institution)
        {
            db.Tertiary_Institution.Add(tertiary_Institution);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tertiary_Institution.TI_ID }, tertiary_Institution);
        }

        // DELETE: api/Tertiary_Institution/5
        [ResponseType(typeof(Tertiary_Institution))]
        public IHttpActionResult DeleteTertiary_Institution(int id)
        {
            Tertiary_Institution tertiary_Institution = db.Tertiary_Institution.Find(id);
            if (tertiary_Institution == null)
            {
                return NotFound();
            }

            db.Tertiary_Institution.Remove(tertiary_Institution);
            db.SaveChanges();

            return Ok(tertiary_Institution);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Tertiary_InstitutionExists(int id)
        {
            return db.Tertiary_Institution.Count(e => e.TI_ID == id) > 0;
        }
    }
}