﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class Hospital_DepartmentController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Hospital_Department
        public IQueryable<Hospital_Department> GetHospital_Department()
        {
            return db.Hospital_Department;
        }

        

        // PUT: api/Hospital_Department/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHospital_Department(int id, Hospital_Department hospital_Department)
        {
           

            if (id != hospital_Department.Hosp_Dept_ID)
            {
                return BadRequest();
            }

            db.Entry(hospital_Department).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Hospital_DepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hospital_Department
        [ResponseType(typeof(Hospital_Department))]
        public IHttpActionResult PostHospital_Department(Hospital_Department hospital_Department)
        {
           

            db.Hospital_Department.Add(hospital_Department);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = hospital_Department.Hosp_Dept_ID }, hospital_Department);
        }

        // DELETE: api/Hospital_Department/5
        [ResponseType(typeof(Hospital_Department))]
        public IHttpActionResult DeleteHospital_Department(int id)
        {
            Hospital_Department hospital_Department = db.Hospital_Department.Find(id);
            if (hospital_Department == null)
            {
                return NotFound();
            }

            db.Hospital_Department.Remove(hospital_Department);
            db.SaveChanges();

            return Ok(hospital_Department);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Hospital_DepartmentExists(int id)
        {
            return db.Hospital_Department.Count(e => e.Hosp_Dept_ID == id) > 0;
        }
    }
}