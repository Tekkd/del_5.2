﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class NursesController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Nurses
        public IQueryable<Nurse> GetNurses()
        {
            return db.Nurses;
        }

     

        // PUT: api/Nurses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNurse(int id, Nurse nurse)
        {
          
            if (id != nurse.Nurse_ID)
            {
                return BadRequest();
            }

            db.Entry(nurse).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NurseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Nurses
        [ResponseType(typeof(Nurse))]
        [AllowAnonymous]
        public IHttpActionResult PostNurse(Nurse nurse)
        {
            nurse.User_ID = "3f5ed4a1-b8c3-4836-bc36-6c464fd7ca8c";
  
            nurse.Branch_ID = 5;
            nurse.Nurse_Status_ID = 1;
            db.Nurses.Add(nurse);
            db.SaveChanges();
            SendEmail(nurse);
            return CreatedAtRoute("DefaultApi", new { id = nurse.Nurse_ID }, nurse);
        }

        // DELETE: api/Nurses/5
        [ResponseType(typeof(Nurse))]
        public IHttpActionResult DeleteNurse(int id)
        {
            Nurse nurse = db.Nurses.Find(id);
            if (nurse == null)
            {
                return NotFound();
            }

            db.Nurses.Remove(nurse);
            db.SaveChanges();

            return Ok(nurse);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NurseExists(int id)
        {
            return db.Nurses.Count(e => e.Nurse_ID == id) > 0;
        }

        public static void SendEmail(Nurse nurse)
        {
            MailMessage mailMessage = new MailMessage("Tekkdsystem@gmail.com", "estervigna10@gmail.com");
            mailMessage.Subject = "New Nurse Application";
            mailMessage.Body = "New application by " + nurse.Nurse_FName + " " + nurse.Nurse_LName +
                " has been submitted, please check the portal";

            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            smtpClient.Credentials = new System.Net.NetworkCredential()
            {
                UserName = "Tekkdsystem@gmail.com",
                Password = "Inf370email"
            };
            smtpClient.EnableSsl = true; 
            smtpClient.Send(mailMessage);



        }
    }
}