﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;
using System.Web.Http.Cors;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    //[EnableCors(origins: "http://localhost:4200/", headers:"*", methods:"*")]
    public class Employee_TypeController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Employee_Type
        public IQueryable<Employee_Type> GetEmployee_Type()
        {
            return db.Employee_Type;
        }

      

        // PUT: api/Employee_Type/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployee_Type(int id, Employee_Type employee_Type)
        {
           

            if (id != employee_Type.Emp_Type_ID)
            {
                return BadRequest();
            }

            db.Entry(employee_Type).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Employee_TypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }  
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Employee_Type
        [ResponseType(typeof(Employee_Type))]
        [HttpPost]
        public IHttpActionResult PostEmployee_Type(Employee_Type employee_Type)
        {
            db.Employee_Type.Add(employee_Type);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employee_Type.Emp_Type_ID }, employee_Type);
        }

        // DELETE: api/Employee_Type/5
        [ResponseType(typeof(Employee_Type))]
        public IHttpActionResult DeleteEmployee_Type(int id)
        {
            Employee_Type employee_Type = db.Employee_Type.Find(id);
            if (employee_Type == null)
            {
                return NotFound();
            }

            db.Employee_Type.Remove(employee_Type);
            db.SaveChanges();

            return Ok(employee_Type);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Employee_TypeExists(int id)
        {
            return db.Employee_Type.Count(e => e.Emp_Type_ID == id) > 0;
        }
    }
}