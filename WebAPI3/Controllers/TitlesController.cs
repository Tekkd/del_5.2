﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class TitlesController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Titles
        public IQueryable<Title> GetTitles()
        {
            //    return db.Titles.Select(x => new Title{ Title_ID =x.Title_ID , Title_Abbreviation = x.Title_Abbreviation}).ToList();
            //using(DBModel db = new DBModel())
            //{
            //    return db.Titles.Select(x => new Title
            //    {
            //        Title_ID = x.Title_ID,
            //        Title_Abbreviation = x.Title_Abbreviation
            //    }).OrderBy(x => x.Title_ID)
            //    .ToArray();
            //}
            return db.Titles;
        }

        

        // PUT: api/Titles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTitle(int id, Title title)
        {
            

            if (id != title.Title_ID)
            {
                return BadRequest();
            }

            db.Entry(title).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TitleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Titles
        [ResponseType(typeof(Title))]
        public IHttpActionResult PostTitle(Title title)
        {
  
            db.Titles.Add(title);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = title.Title_ID }, title);
        }

        // DELETE: api/Titles/5
        [ResponseType(typeof(Title))]
        public IHttpActionResult DeleteTitle(int id)
        {
            Title title = db.Titles.Find(id);
            if (title == null)
            {
                return NotFound();
            }

            db.Titles.Remove(title);
            db.SaveChanges();

            return Ok(title);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TitleExists(int id)
        {
            return db.Titles.Count(e => e.Title_ID == id) > 0;
        }
    }
}