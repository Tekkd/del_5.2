﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    [AllowAnonymous]
    public class Postal_CodeController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Postal_Code
        public IQueryable<Postal_Code> GetPostal_Code()
        {
            return db.Postal_Code;
        }

       

        // PUT: api/Postal_Code/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPostal_Code(int id, Postal_Code postal_Code)
        {
         

            if (id != postal_Code.Pos_Code)
            {
                return BadRequest();
            }

            db.Entry(postal_Code).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Postal_CodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Postal_Code
        [ResponseType(typeof(Postal_Code))]
        public IHttpActionResult PostPostal_Code(Postal_Code postal_Code)
        {
            db.Postal_Code.Add(postal_Code);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = postal_Code.Pos_Code }, postal_Code);
        }

        // DELETE: api/Postal_Code/5
        [ResponseType(typeof(Postal_Code))]
        public IHttpActionResult DeletePostal_Code(int id)
        {
            Postal_Code postal_Code = db.Postal_Code.Find(id);
            if (postal_Code == null)
            {
                return NotFound();
            }

            db.Postal_Code.Remove(postal_Code);
            db.SaveChanges();

            return Ok(postal_Code);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Postal_CodeExists(int id)
        {
            return db.Postal_Code.Count(e => e.Pos_Code == id) > 0;
        }
    }
}