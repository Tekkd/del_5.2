﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI3.Models;

namespace WebAPI3.Controllers
{
    public class ConfirmNurseController : ApiController
    {
        DBModel db = new DBModel();
        [AllowAnonymous]
        [HttpGet]
        [Route("api/NurseList")]
    public List<Nurse> GetNurse()
        {

            return db.Nurses.Where(x => x.Nurse_Status_ID == 1).ToList();

        }
        [HttpGet]
        [Route("api/Rank/{id}")]
        public string getRank(int id)
        {

            return db.Nurse_Rank.Find(id).Rank_Name;
        }
    }
}
