use Nurses_R_Us
Go


INSERT INTO Nurse_Rank VALUES('Registered Nurses','A Registered nurse / midwife / accoucheur may be registered in more than one category.');
INSERT INTO Nurse_Rank VALUES('Registered Midwives','A Registered nurse / midwife / accoucheur may be registered in more than one category.'); 
INSERT INTO Nurse_Rank VALUES('Enrolled Nurses', 'White epaulette and maroon badge worn by an Enrolled Nurse.');
INSERT INTO Nurse_Rank VALUES('Enrolled Midwives','White epaulette and green badge worn by an Enrolled Midwife.');
INSERT INTO Nurse_Rank VALUES('Enrolled Nursing Auxiliaries','Sometimes, the blue enamel used on this brooch is so dark that it appears almost black');